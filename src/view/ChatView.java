package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import sockets.TCPClient;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChatView extends JFrame {

	private JPanel contentPane;
	private JTextField ipTb;
	private JTextField puertoTb;
	private JTextField mensajeTb;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatView frame = new ChatView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChatView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 460);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblIp = new JLabel("IP");
		lblIp.setBounds(42, 37, 46, 14);
		contentPane.add(lblIp);

		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setBounds(42, 78, 46, 14);
		contentPane.add(lblPuerto);

		JTextArea historialTa = new JTextArea();
		historialTa.setBounds(42, 118, 366, 194);
		contentPane.add(historialTa);

		ipTb = new JTextField();
		ipTb.setBounds(98, 34, 202, 20);
		contentPane.add(ipTb);
		ipTb.setColumns(10);

		puertoTb = new JTextField();
		puertoTb.setBounds(98, 75, 202, 20);
		contentPane.add(puertoTb);
		puertoTb.setColumns(10);

		mensajeTb = new JTextField();
		mensajeTb.setBounds(42, 348, 238, 20);
		contentPane.add(mensajeTb);
		mensajeTb.setColumns(10);

		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String ip = ipTb.getText();
				int puerto = Integer.parseInt(puertoTb.getText());
				String mensaje = mensajeTb.getText();
				String mensajeDeServidor = TCPClient.enviarMensaje(ip, puerto, mensaje);
				historialTa.append(mensajeDeServidor + "\n");
			}
		});
		btnEnviar.setBounds(308, 347, 89, 23);
		contentPane.add(btnEnviar);
	}
}

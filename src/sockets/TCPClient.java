package sockets;

import java.io.*;
import java.net.*;

public class TCPClient {
	public static String enviarMensaje(String ip, int puerto, String mensaje) {
		try {
			String sentenceFromServer; // Almacena la respuesta del servidor
			BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
			Socket clientSocket = new Socket(ip, puerto);
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			outToServer.writeBytes(mensaje + "\n");
			sentenceFromServer = inFromServer.readLine();
			System.out.println("FROM SERVER: " + sentenceFromServer);
			clientSocket.close();
			return sentenceFromServer;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String argv[]) throws Exception {
		String sentence; // Almacena la cadena que se enva al servidor
		String modifiedSentence; // Almacena la respuesta del servidor
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Socket clientSocket = new Socket("172.16.10.27", 6789);
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		sentence = inFromUser.readLine();
		outToServer.writeBytes(sentence + "\n");
		modifiedSentence = inFromServer.readLine();
		System.out.println("FROM SERVER: " + modifiedSentence);
		clientSocket.close();
	}
}



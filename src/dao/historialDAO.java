package dao;
import entities.historial;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import conexion.MysqlConnect;

public class historialDAO implements IhistorialDAO{

MysqlConnect con;
	
	public historialDAO(){
		con = new MysqlConnect();
		
		con.connect();
	}

	@Override
	public void agregar(historial hi) {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = con.getCon().prepareStatement("INSERT INTO historial"
																		+ "(puerto, ip, mensaje)" 
																		+ "VALUES(?,?,?)");
			statement.setString(1, hi.getPuerto());
			statement.setInt(2, hi.getIp());
			statement.setString(3, hi.getMensaje());
			
			statement.executeUpdate();
			
			System.out.println("A�adido con exito :)");
			
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}

